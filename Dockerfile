# Strapi version
ARG VERSION=3.0.0-alpha.13.1

# Build
FROM node:10-slim as build

ENV VERSION=${VERSION}
WORKDIR /build

COPY strapi-app ./strapi-app

RUN npm i -g strapi@${VERSION} recursive-install
RUN npm-recursive-install --skip-root --production

COPY strapi.sh ./
COPY healthcheck.js ./
RUN chmod +x ./strapi.sh

# Prod
FROM node:10-alpine

ENV VERSION=${VERSION}

LABEL maintainer="Mario Lopez <mario@techmunchies.net>" \
      org.label-schema.vendor="techmunchies" \
      org.label-schema.name="West Point Real Estate API using Strapi" \
      org.label-schema.description="West Point Real Estate API containerized" \
      org.label-schema.url="https://strapi.io" \
      org.label-schema.vcs-url="https://gitlab.com/tmAppDev/westpoint-api" \
      org.label-schema.version=latest \
      org.label-schema.schema-version="1.0"

WORKDIR /usr/src/api
COPY --from=build /build .
RUN npm i -g strapi@${VERSION}
      
EXPOSE 1337

HEALTHCHECK --interval=15s --timeout=5s --start-period=30s \
      CMD node /usr/src/api/healthcheck.js

CMD ["./strapi.sh"]
